/* Watch - A custom Casio F91-W motion
 * Copyright (C) 2024  Hayley Hughes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "msp430fr6972.h"

int configure_lcd(void) {
	// Uses ACLK by default
	
	// Turn on LCD segments
	LCDCCTL0 |= 1 << 2;

	// Use low power waveforms
	// TODO: evaluate the performance between standard and low power
	// mode
	LCDCCTL0 |= 1 << 1;

	// Clear the LCD memory
	LCDCMEMCTL |= 3 << 1;

	// Defaults to using internal LCD bias resistors
	
	// Defaults to VLCD being sourced internally
	
	// Enable the LCD charge pump
	LCDCVCTL |= 1 << 3;
	LCDCVCTL |= 1 << 9;

	// Select 3-mux mode
	LCDCCTL0 |= 2;

	// 1/3 bias selected by default
	
	// Enable LCD segment pins
	LCDCPCTL0 = 0b1000011111111111;
	LCDCPCTL1 = 0b0000111101111111;

	// Turn on the LCD
	LCDCCTL0 |= 1;
}
