/* Watch - A custom Casio F91-W motion
 * Copyright (C) 2024  Hayley Hughes
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "msp430fr6972.h"
#include "lcd.h"

// Configure the system clocks
// 
// Returns 0 on success or non 0 if there has been an LXFT oscillator fault
int configure_clk(void) {
	// Unlock clock select registers
	CSCTL0 = CSKEY;

	// MCLK (CPU clock) defaults to DCO, 8MHz
	
	// ACLK defaults to LXFTCLK
	
	// VLOCLK disabled by default
	
	// Disable SMCLK
	CSCTL4 |= 1 << 1;

	// LXFTCLK used as ACLK so is enabled by default
	
	// TODO: figure out the required drive level
	CSCTL4 |= 3 << 6;

	// Wait for the clock configuration to take effect
	while ((CSCTL5 >> 2) & 1);

	return CSCTL5 & 1;
}

int main(void) {
	return 0;
}

